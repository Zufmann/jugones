import './Modal.css'

import React, { PureComponent } from 'react'

class Modal extends PureComponent {

  render() {
    return (
      <div className="backdrop">
        <div className="modal">
          {this.props.children}
          <div className="buttons">
            <button className="closeButton" onClick={this.props.onClose}>
              Cerrar
            </button>
            {this.props.extraButtons}
          </div>
        </div>
      </div>
    )
  }
}

export default Modal
