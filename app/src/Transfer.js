import './Transfer.css'

import React, { PureComponent } from 'react'

class Transfer extends PureComponent {

  render() {
    const {teams, team, players, player, error} = this.props;
    return (
      <div className="transferModal">
        <select
          onChange={(event) => {
            this.props.handleTransferChange('team', event);
          }}
          value={team}
        >
          <option value="" disabled selected>Select your option</option>
          {teams.map(team => (
            <option
              key={team.id}
              value={team.id}
            >{team.name}
            </option>
          ))}
        </select>
        {team && (
          <div>
            Presupuesto: {teams.find(currentTeam => currentTeam.id === team).money}
          </div>
        )}
        <select
          onChange={(event) => {
            this.props.handleTransferChange('player', event);
          }}
          value={player}
        >
          <option value="" disabled selected>Select your option</option>
          {players.map(player => (
            <option
              key={player.id}
              value={player.id}
            >{player.name}
            </option>
          ))}
        </select>
        {error && (
          <div>
            {error}
          </div>
        )}
      </div>
    )
  }
}

export default Transfer
