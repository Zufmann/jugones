import './PlayerCard.css'

import React, { PureComponent } from 'react'

class PlayerCard extends PureComponent {

  compactMode() {
    const { player, team } = this.props;
    return (
      <div
        key={player.name}
        className="compactPlayerCard"
        onClick={this.props.toggleTransfer}
      >
        <div className="compactCardRow">
          <div className="playerData">
            <div className="compactImageCropper">
              <img src={player.img} className="playerImg"/>
            </div>
          </div>
          <div className="playerData">
            <div>{player.name}</div>
            <div className="playerPosition">{player.position}</div>
          </div>
          <div className="teamName">{team.name}</div>
        </div>
        <div className="playerCrest">
          <img src={team.shield} className="teamCrest"/>
        </div>
      </div>
    )
  }

  normalMode() {
    const { player, team } = this.props;
    return (
      <div
        key={player.name}
        className="playerCard"
        onClick={this.props.toggleTransfer}
      >
        <div className="cardRow">
          <div className="playerData">
            <div className="imageCropper">
              <img src={player.img} className="playerImg"/>
            </div>
          </div>
          <div className="playerData">
            <div>{player.name}</div>
            <div className="playerPosition">{player.position}</div>
          </div>
          <div className="teamName">{team.name}</div>
        </div>
        <div className="playerCrest">
          <img src={team.shield} className="teamCrest"/>
        </div>
      </div>
    )
  }

  render() {
    return this.props.compactMode? this.compactMode():this.normalMode();
  }
}

export default PlayerCard
