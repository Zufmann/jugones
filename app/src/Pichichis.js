import React from 'react'
import FontAwesome  from 'react-fontawesome'
import './Pichichis.css'

const Pichichis = ({pichichis, goalSort, toggleSort}) => {
  return (
    <table className="">
      <tr>
        <th>Jugador</th>
        <th onClick={toggleSort}>Goles
          <FontAwesome
            className="icon"
            name={goalSort? "sort-asc": goalSort === false? "sort-desc":"sort"}
          />
        </th>
      </tr>
      {pichichis.map(player => (
        <tr key={player.name}>
          <td>{player.name}</td>
          <td>{player.goals}</td>
        </tr>
      ))}
    </table>
  );
};

export default Pichichis;