import './App.css'

import React, { PureComponent } from 'react'
import PlayerCard from "./PlayerCard";
import Pichichis from "./Pichichis";
import Modal from "./Modal";
import Transfer from "./Transfer";
const domain = 'http://localhost:3001';
const PICHICHIS = 'Pichichis';
const InitialState = {
    players: [],
    teams: [],
    pichichis: [],
    transferPlayers: [],
    goalSort: undefined,
    cardModeCompact: false,
    pichichiToggle: false,
    transfer: undefined
  };

class App extends PureComponent {
  constructor(props) {
    super(props);
    this.state = localStorage.getItem("appState") ? JSON.parse(localStorage.getItem("appState")) : InitialState;

  }

  componentDidMount() {
    this.load('players');
    this.load('teams');
    this.load('pichichis');
    window.addEventListener('resize', this.selectCardType);
    this.selectCardType()
  }

  load = (endpoint) => {
    fetch(`${domain}/${endpoint}`)
      .then(response => {
        return response.json();
      })
      .then(jsonResponse => {
        this.setState({ [endpoint]: jsonResponse })
      })
      .catch(e => {});
  };

  componentWillUnmount() {
    window.removeEventListener('resize', this.selectCardType);
  }

  componentDidUpdate() {
    localStorage.setItem('appState', JSON.stringify(this.state));
  }

  selectCardType = () => {
    this.setState({ cardModeCompact: window.innerWidth < 600 });
  };

  togglePichichis = () => {
    this.setState(state => ({pichichiToggle: !state.pichichiToggle}))
  };

  toggleTransfer = () => {
    this.setState(state => ({
      transfer: !state.transfer,
      transferPlayers: state.players,
      team: undefined,
      player: undefined,
      transferError: undefined
    }))
  };

  handleTransferChange = (attrib, event) => {
    const attribId = event.target.value;
    this.setState(state => {
      let transferPlayers = state.transferPlayers;
      if(attrib === 'team'){
        transferPlayers = state.players.filter(player => player.teamId !== attribId)
      }
      return {[attrib]: attribId, transferPlayers}
    });
  };

  toggleSort = () => {
    this.setState(state => {
      const goalSort = !state.goalSort;
      const direction = goalSort? 1:-1;
      const pichichis = state.pichichis.sort((playerA, playerB) => {
        return direction * (playerB.goals - playerA.goals)
      });
      return {goalSort, pichichis}
    })
  };

  transferAcceptButtons = () => {
    return this.state.team && this.state.player && (
      <button className="closeButton" onClick={this.transfer}>
        Transferir
      </button>
    );
  };

  transfer = () => {

    this.setState({transferError: undefined});
    fetch(`${domain}/transfer`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        playerId: this.state.player,
        teamId: this.state.team,
      })
    })
    .then(response => {
      return response.json();
    })
    .then(jsonResponse => {
      if(jsonResponse.error) {
        this.setState({transferError: jsonResponse.message})
      } else {
        this.load('players');
      }
    })
    .catch(e => {});
  };

  render() {
    const { players, teams, cardModeCompact, pichichiToggle } = this.state;

    return <div className="App">
      {pichichiToggle &&
        <Modal onClose={this.togglePichichis}>
          <Pichichis
            pichichis={this.state.pichichis}
            goalSort={this.state.goalSort}
            toggleSort={this.toggleSort}
          />
        </Modal>
      }
      {this.state.transfer &&
        <Modal
          onClose={this.toggleTransfer}
          extraButtons={this.transferAcceptButtons()}
        >
          <Transfer
            players={this.state.transferPlayers}
            teams={teams}
            player={this.state.player}
            team={this.state.team}
            error={this.state.transferError}
            handleTransferChange={this.handleTransferChange}
          />
        </Modal>
      }
      <header className="App-heading App-flex">
        <button className="pichichiButton"
             onClick={this.togglePichichis}>{PICHICHIS}</button>
      </header>
      <div className="App-players App-flex">
        {/* 
          TODO ejercicio 2
          Debes obtener los players en lugar de los equipos y pintar su nombre. 
          Borra todo el código que no sea necesario. Solo debe existir un título: Los jugadores
          y una lista con sus nombres. 
          ** Los comentarios de los ejercicios no los borres.
        */}
        <div className="playersLarge">
          {/* 
            TODO ejercicio 3
            Vamos a pasar a darle diseño. Crea el diseño propuesto en el readme con los requerimientos que se necesite.
            Guiate por las imágenes.
           */}
          {teams.length > 0 && players.map(player => {
            const team = teams.find(team => team.id === player.teamId);
            return <PlayerCard
              key={player.name}
              player={player}
              team={team}
              compactMode={cardModeCompact}
              toggleTransfer={this.toggleTransfer}
            />
            }
          )}
        </div>
      </div>
    </div>
  }
}

export default App
